# Trigger TensorFlow

TensorFlow will trigger when Qbo sees your face (blue nose) and you says one of this sentences:

## Spanish

- "mira esto"
- "observa esto"
- "reconocimiento visual"
- "inicia reconocimiento visual"
- "que ves"
- "que tengo aqui"

## English

- "watch this"
- "visual recognition"
- "start visual recognition"
- "look this"
- "what is this"

After that, Qbo will take a picture of what he sees and after some seconds he'll say what he saw.

## Change/Add Hotword...

You can add or change the hotword in the ./VisualRecognation.py file. In the `askAboutMe` function.