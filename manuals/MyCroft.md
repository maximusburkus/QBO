# MyCroft Config

## Installation

0.- Select MyCroft in http://qbo.local:8000/settings and Reboot, 

1.- After that you'll hear MyCroft saying the Pairing code, write the code that you'll hear, we'll need it later. If you does not understand a letter, wait a few seconds and Qbo will repeat it again.

2.- Go to https://mycroft.ai and login.

3.- Up-right click on the arrow next to your name and select "Devices", after that add a new device. Here you need to introduce your code obtained by Qbo/Mycroft. If everything goes fine, Qbo will talk saying that the pairing process has been completed, and you're ready to use it

## Usage

If you look at Qbo, MyCroft will trigger.
