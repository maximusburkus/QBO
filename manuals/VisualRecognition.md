# Visual Recognition (Command)

To use the visual recognition it is necessary to access via console to the QBO.

First, access the web interface settings and set QBO to start in development mode. Then click on Save and restart.

Wait for QBO to start.

Access QBO via SSH or VNC and open a terminal.

To start visual recognition execute the following command:

```bash
sudo -u qbo /opt/qbo/VisualRecognition.py
```

Note: For IBM Watson Visual Recognition you need API Key set in Settings.
